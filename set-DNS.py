import csv
import sys
import argparse
import paramiko
import getpass
import logging 

#Configure and commence logging

logger = logging.getLogger(__name__)
logging.basicConfig(filename='python.log', level=logging.DEBUG)
logger.debug('logging start')

#Set up argument parsing

parser = argparse.ArgumentParser(prog="set-DNS.py", description="set DNS on iLO")

parser.add_argument('-o', '--read', help="Read only mode. Does not make changes. Currently the only mode that does anything", action='store_true')
parser.add_argument('-v', '--verbose')

args = parser.parse_args()

print(args)
print(args.read)

with open('hosts-in.csv',encoding='utf8') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',')
    next(csvreader)
    USER_NAME = input("Please enter BMC username:")
    PASSWORD=getpass.getpass()
    i = 1
    for row in csvreader:
        print(i)
        i += 1
        BMC_NAME = (row[1])
        print(BMC_NAME)
        if i > 0:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=BMC_NAME, look_for_keys=False, port=22, username=USER_NAME, password=PASSWORD, disabled_algorithms={'pubkeys': ['rsa-sha2-256', 'rsa-sha2-512']})
            DNSout = ("show map1/settings1/DNSSettings1")
            #SetDNS = ("set map1/settings1/DNSSettings1 DNSServerAddress=0.0.0.0") 
            #SetDNSDomain = ("set map1/settings1/DNSSettings1 DomainName=") 
            
            if args.read:
                stdin, stdout, stderr = client.exec_command(DNSout)
                print(stdout.read())
            #else:
                #stdin, stdout, stderr = client.exec_command(SetDNS)
                #print(stdout.read())


