import csv
import argparse
import paramiko
import getpass
import logging
import re 

#Configure and commence logging

logger = logging.getLogger(__name__)
logging.basicConfig(filename='python.log', level=logging.DEBUG)
logger.debug('logging start')

with open('hosts-in.csv',encoding='utf8') as csvfile,open('MACs.csv','w') as csvfile_out:
    csvreader = csv.reader(csvfile, delimiter=',')
    csvwriter = csv.writer(csvfile_out)
    next(csvreader)
    USER_NAME = input("Please enter BMC username:")
    PASSWORD=getpass.getpass()
    i = 1
    P1_PATTERN=re.compile('Port1NIC_MACAddress=\w{2}:\w{2}:\w{2}:\w{2}:\w{2}:\w{2}')
    P2_PATTERN=re.compile('Port2NIC_MACAddress=\w{2}:\w{2}:\w{2}:\w{2}:\w{2}:\w{2}')
    P3_PATTERN=re.compile('Port3NIC_MACAddress=\w{2}:\w{2}:\w{2}:\w{2}:\w{2}:\w{2}')
    for row in csvreader:
        print(i)
        BMC_NAME = (row[1])
        SYSTEM_NAME = (row[0])
        print(BMC_NAME)
        if i > 0:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=BMC_NAME, look_for_keys=False, port=22, username=USER_NAME, password=PASSWORD, disabled_algorithms={'pubkeys': ['rsa-sha2-256', 'rsa-sha2-512']})
            #command = ("OemHPE_licenseinstall /map1/oemHPE_license1 "+LICENSE_KEY)
            command = ("show /system1/network1/Integrated_NICs")
            stdin, stdout, stderr = client.exec_command(command)
            NET_INFO=stdout.read()
            P1_MAC=(str(P1_PATTERN.findall(str(NET_INFO)))[-19:-2])
            P2_MAC=(str(P2_PATTERN.findall(str(NET_INFO)))[-19:-2])
            P3_MAC=(str(P3_PATTERN.findall(str(NET_INFO)))[-19:-2])
            csvwriter.writerow([SYSTEM_NAME,P1_MAC,P2_MAC,P3_MAC])

        i += 1
