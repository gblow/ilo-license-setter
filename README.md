This repository contains a number of scripts that are designed to interact with HPE iLO BMCs (Hewlett Packard Enterprises, integrated Lights Out, Baseboard Management Controllers ) to access information and provide limited actuation. If usage information is not provided through the help text for each script, care should be taken when determining what the script will do before running. 

Recommend to set up a virtual env for python to load appropriate modules (with e.g. `python3 -m venv venv`)

load the venv by sourcing the activate file (e.g. `source venv/bin/activate`)

install requirements manually

`python3 -m pip install paramiko`
