import csv
import argparse
import paramiko
import getpass
import logging
import re 

#Configure and commence logging

logger = logging.getLogger(__name__)
logging.basicConfig(filename='python.log', level=logging.DEBUG)
logger.debug('logging start')

#Set up argument parsing

parser = argparse.ArgumentParser("get-serials.py")
parser.add_argument(help="This script will access every host BMC in the hosts-in.csv list and retrieve the serial number for them, writing them out to a csv file, along with the system host name for identification.")

with open('hosts-in.csv',encoding='utf8') as csvfile,open('serials.csv','w') as csvfile_out:
    csvreader = csv.reader(csvfile, delimiter=',')
    csvwriter = csv.writer(csvfile_out)
    next(csvreader)
    USER_NAME = input("Please enter BMC username:")
    PASSWORD=getpass.getpass()
    i = 1
    SN_PATTERN=re.compile('number=\w{10}')
    for row in csvreader:
        print(i)
        BMC_NAME = (row[1])
        SYSTEM_NAME = (row[0])
        print(BMC_NAME)
        if i > 0:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=BMC_NAME, look_for_keys=False, port=22, username=USER_NAME, password=PASSWORD, disabled_algorithms={'pubkeys': ['rsa-sha2-256', 'rsa-sha2-512']})
            #command = ("OemHPE_licenseinstall /map1/oemHPE_license1 "+LICENSE_KEY)
            command = ("show /system1 number")
            stdin, stdout, stderr = client.exec_command(command)
            SERIAL_NUMBER_OUT=stdout.read()
            SERIAL_NUMBER=(str(SN_PATTERN.findall(str(SERIAL_NUMBER_OUT)))[-12:-2])
            csvwriter.writerow([SYSTEM_NAME,SERIAL_NUMBER])

        i += 1
