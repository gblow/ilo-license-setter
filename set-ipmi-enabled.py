import csv
import argparse
import paramiko
import getpass
import logging 

#Configure and commence logging

logger = logging.getLogger(__name__)
logging.basicConfig(filename='python.log', level=logging.DEBUG)
logger.debug('logging start')

#Set up argument parsing

parser = argparse.ArgumentParser("set-ipmi-enabled.py")
parser.add_argument(help="This script will take every host BMC in the hosts-in.csv list and set map1/config1 oemHPE_ipmi_dcmi_overlan_enable=yes on it.")

with open('hosts-in.csv',encoding='utf8') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',')
    next(csvreader)
    USER_NAME = input("Please enter BMC username:")
    PASSWORD=getpass.getpass()
    i = 1
    for row in csvreader:
        print(i)
        i += 1
        BMC_NAME = (row[1])
        print(BMC_NAME)
        if i > 0:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=BMC_NAME, look_for_keys=False, port=22, username=USER_NAME, password=PASSWORD, disabled_algorithms={'pubkeys': ['rsa-sha2-256', 'rsa-sha2-512']})
            command = ("set map1/config1 oemHPE_ipmi_dcmi_overlan_enable=yes")
            stdin, stdout, stderr = client.exec_command(command)
            print(stdout.read())


