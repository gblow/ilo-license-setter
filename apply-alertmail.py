import csv
import argparse
import paramiko
import getpass
import logging 

#Configure and commence logging

logger = logging.getLogger(__name__)
logging.basicConfig(filename='python.log', level=logging.DEBUG)
logger.debug('logging start')

with open('hosts-in.csv',encoding='utf8') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',')
    next(csvreader)
    USER_NAME = input("Please enter BMC username:")
    PASSWORD=getpass.getpass()
    i = 1
    for row in csvreader:
        print(i)
        i += 1
        BMC_NAME = (row[1])
        #This is legacy and needs a new column setting, depending on how implemented in future case. Should be quick to determine so leaving to do until relevant.
        #LICENSE_KEY = (row[7])
        print(BMC_NAME)
        if i > 0:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=BMC_NAME, look_for_keys=False, port=22, username=USER_NAME, password=PASSWORD, disabled_algorithms={'pubkeys': ['rsa-sha2-256', 'rsa-sha2-512']})
            showCommand = ("show /map1/oemHPE_alertmail1")
            setDest = ("set /map1/oemHPE_alertmail1 oemHPE_alertmail_email=iris-somerville-admin@mlist.is.ed.ac.uk")
            setServer = ("set /map1/oemHPE_alertmail1 oemHPE_alertmail_smtp_server=10.19.1.40")
            setSender = ("set /map1/oemHPE_alertmail1 oemHPE_alertmail_sender_domain=ilo-alertmail@somerville.ed.ac.uk")
            setPort = ("set /map1/oemHPE_alertmail1 oemHPE_alertmail_smtp_port=587")
            setSecure = ("set /map1/oemHPE_alertmail1 oemHPE_alertmail_smtp_secure_enable=yes")
            setEnable = ("set /map1/oemHPE_alertmail1 oemHPE_alertmail_enable=yes")
            stdin, stdout, stderr = client.exec_command(showCommand)
            print(stdout.read())
            stdin, stdout, stderr = client.exec_command(setDest)
            print(stdout.read())
            stdin, stdout, stderr = client.exec_command(setServer)
            print(stdout.read())
            stdin, stdout, stderr = client.exec_command(setSender)
            print(stdout.read())
            stdin, stdout, stderr = client.exec_command(setPort)
            print(stdout.read())
            stdin, stdout, stderr = client.exec_command(setSecure)
            print(stdout.read())
            stdin, stdout, stderr = client.exec_command(setEnable)
            print(stdout.read())
            stdin, stdout, stderr = client.exec_command(showCommand)
            print(stdout.read())